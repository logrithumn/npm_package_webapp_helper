{
  "extends": [
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:react/recommended",
    "plugin:jsx-a11y/recommended",
    "plugin:react/jsx-runtime"
  ],
  "plugins": ["react", "import", "jsx-a11y"],
  "parserOptions": {
    "ecmaVersion": 2021,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "es6": true,
    "browser": true,
    "node": true
  },
  "settings": {
    "react": {
      "version": "detect"
    },
    "import/resolver": {
      "node": {
        "paths": ["src"]
      },
      "alias": {
        "map": [
          ["koc", "./node_modules/@koc"]
        ],
        "extensions": [".js", ".jsx", ".json"]
      }
    }
  },
  "ignorePatterns": [
    "node_modules/"
  ],
  "rules": { //https://eslint.org/docs/latest/rules/
    "jsx-a11y/label-has-associated-control": [ 2, {
      "controlComponents": ["Checkbox"],
      "depth": 3
    }],
    "max-len": ["error", {
      "code": 80,
      "tabWidth": 2,
      "ignorePattern": "^\\s*import\\s.+from\\s*"
    }],
    "max-lines": ["error", {"max": 300}],
    "max-depth": ["error", 2],
    "react/prop-types": 1,
    "indent": [
      "error",
      2,
      {
        "VariableDeclarator": 1,
        "MemberExpression": 0,
        "ObjectExpression": 1
      }
    ],
    "linebreak-style": ["error", "unix"],
    "quotes": ["error", "single", { "allowTemplateLiterals": true }],
    "eqeqeq": ["error", "always"],
    "func-call-spacing": ["error", "never"],
    "implicit-arrow-linebreak": ["error", "beside"],
    "brace-style": ["error", "stroustrup"],
    "comma-dangle": ["error", {
      "arrays": "only-multiline",
      "objects": "always-multiline",
      "imports": "always-multiline",
      "exports": "never",
      "functions": "never"
    }],
    "key-spacing": ["error", {
      "beforeColon": false,
      "afterColon": true,
      "mode": "strict"
    }],
    "no-mixed-spaces-and-tabs": "error",
    "semi": 2,
    "no-extra-semi":"error",
    "semi-spacing": ["error", {"before": false, "after": true}],
    "no-magic-numbers": ["error", {"ignore": [-1, 0, 1, 2, 100]}]
  }
}
