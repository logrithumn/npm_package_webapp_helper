import PropTypes from 'prop-types';

const Layout = function (props = {}) {
  return (
    <>
      {props.children}
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;