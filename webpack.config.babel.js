const webpack = require('webpack');
const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const isProduction = process.env.NODE_ENV === 'production';
const ENV = isProduction ? 'production' : 'development';
const PORT = process.env.PORT || 3000;
const DIR_NAME = process.cwd();

const config = {
  context: path.resolve(DIR_NAME, 'src'),
  entry: [
    './index'
  ],
  mode: ENV,
  output: {
    path: path.resolve(DIR_NAME, 'dist'),
    publicPath: '',
    filename: isProduction ? '[name].[chunkhash:5].js': '[name].js',
    chunkFilename: '[name].chunk.[chunkhash:5].js',
    assetModuleFilename: '[path][name][ext][query]'
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json', '.styl', '.less', '.css'],
    modules: [
      path.resolve(DIR_NAME, 'node_modules'),
      'node_modules'
    ],
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: path.resolve(DIR_NAME, 'src'),
        enforce: 'pre',
        use: 'source-map-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: [/node_modules\/(?!(@koc)\/).*/, /\.test\.js(x)?$/],
        use: {
          loader: 'babel-loader',
          options: {
            configFile: path.resolve(__dirname, '.babelrc')
          }
        }
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        enforce: 'pre',
        test: /\.styl$/,
        use: 'stylus-loader'
      },
      {
        enforce: 'pre',
        test: /\.less$/,
        use: 'less-loader',
      },
      {
        test: /\.(styl|css|less)$/,
        include: [
          path.resolve(DIR_NAME, 'src', 'js', 'components'),
          path.resolve(DIR_NAME, 'src', 'js', 'routes'),
        ],
        use: ['css-loader', 'postcss-loader'],
      },
      {
        test: /\.(css|less|s[ac]ss|styl)$/,
        exclude: [
          path.resolve(DIR_NAME, 'src', 'js', 'components'),
          path.resolve(DIR_NAME, 'src', 'js', 'routes'),
        ],
        use: ['css-loader', 'postcss-loader'],
      },
      {
        test: /\.(xml|html|txt|md)$/,
        use: 'raw-loader'
      },
      {
        test: /\.(svg|woff2?|ttf|eot|jpe?g|png|gif)(\?.*$|$)/i,
        type: 'asset/resource'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.ejs',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      },
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      process: {
        env: {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        }
      },
    }),
    new ESLintPlugin({
      overrideConfigFile: path.resolve(__dirname, '.eslintrc'),
      extensions: ['jsx', 'js'],
      exclude: ['node_modules'],
    })
  ],
  stats: { colors: true },
  node: {
    global: true,
  },
  devtool: 'cheap-module-source-map',
  devServer: {
    static: {
      directory: path.join(DIR_NAME, 'dist'),
      watch: false,
    },
    compress: true,
    port: PORT,
    open: ['/']
  }
}

module.exports = config;